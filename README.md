# tekartik_pedantic.dart

Common pedantic rules

## Usage

In your `pubspec.yaml`:

```yaml
dependencies:
  tekartik_pedantic:
    git:
      url: ssh://git@gitlab.com/tekartik/pedantic.dart
      ref: dart2
    version: '>=0.1.0'
```

In your `analysys_options.yaml`:

```yaml
# tekartik analysis_options extension to pedantic
include: package:tekartik_pedantic/analysis_options.app.strong_mode.yaml
```

## Documentation

* [Pedantic](https://github.com/tekartik/common_utils.dart/blob/master/doc/pedantic.md)

